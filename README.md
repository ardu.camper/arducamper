<div align="center">
<img src="https://gitlab.com/ardu.camper/arducamper/raw/master/readme/logo_transparent.png" alt="ArduCamper logo">
</div>

---

ArduCamper is package of open source hardware and software tools, to provide you possibility to monitor status of your RV via bluetooth and android app.

# Main Features
* Android Monitoring App
    * 4 Gauges
    * 2 Charts (12h history)
    * Custom names, ranges, units
    * Custom color
    * Portrait and landscape mode
    * Auto-connect and reconnect function
    * Demo mode
* Arduino code
    * Setting is stored in EEPROM
    * Can measure up to
        * 4 temperatures with DS18B20 sensors
        * 4 Analog input (battery measurement) + temperature correction
    * Analog inputs with custom resistor dividers, and divider
    * AI can be configured for minimum and maximal values to return percentage
* ArduCamper PCB
    * Supply 12V
    * Connection of all inputs used in Arduino Code
    * HW is ready for (not used in code)
        * I2C bus
        * Digital outputs
        * Signal LEDs
* Android configuration app
    * Can fully configure inputs and outputs used for measurement
    * Asign temperature sensors
    * Asign gauges and charts for app
    * Set AI voltage dividers
    * Set minimal and maximal values for HX711
    * Some debug options

# Readme

Project documentation and how to guide can be found on project [Wiki](https://gitlab.com/ardu.camper/arducamper/-/wikis/home)
