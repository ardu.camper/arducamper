//==================================================================================================
// include
//==================================================================================================

#include <EEPROM.h>
#include <OneWire.h>

//==================================================================================================
// definitions
//==================================================================================================

#define OUT0 7
#define OUT1 8

//create OneWire bus on PIN12
OneWire ds(12);

//==================================================================================================
// HW Definitions
//==================================================================================================

//------------------------------- A N A L O G   I N P U T S ----------------------------------------
/*
reference voltage = 5V
AI 10 bit = 1024 values

without resitor divider
Voltage = (AI*5)/1024 = AI/204.8
Divider = 204.8

With divider
Voltage = (R1 + R2)/R2 * (AI*5)/1024 = AI/(204.8 * R2/(R1+R2) )
Divider = 204.8 * R2/(R1+R2)

Default R1=330k, R2=47k
Divider = 204.8 * 47/(330+47) = 25.532

*/
const float SET_AI_DIVIDERS[]={       //voltage divider for AI
    204.8,
    25.532,
    25.532,
    25.532
  };

const float SET_AI_TEMP_COMP[]={      //temp compensation for AI (computed from TEMP1 sensor)
    0.0057,
    0,
    0,
    0
};

//==================================================================================================
// global variables
//==================================================================================================

unsigned long timerStart = 0;     //timer ms counter
int counter = 0;                  //counter for main loop
int DataIndex = 0;                //index of current data to work with
byte DataSource[6];               //source of data - read from EEPROM to avoid continues reading
float DataValues[6];              //current values of configured sensors
float Data5_h[24];                //historical data for 12h (30m each)
float Data6_h[24];
int i;                            //for various loops
String SerialBuffer = "";         //serial buffer to read messages
float AI_min[4];                  //range for AI (if shown as percetage)
float AI_max[4];                  //range for AI

//==================================================================================================
// constants
//==================================================================================================
const unsigned long DELAY_TIME = 100;                     //delay of main loop (non blocking) 100ms
const char FW_VERSION[] = "2.0.0";                        //FW version
const char FW_BUILD_DATETIME[] = __DATE__ " " __TIME__;   //buld datetime

//==================================================================================================
// Setup
//==================================================================================================

void setup() {

  //init outputs
  pinMode(OUT0, OUTPUT);
  pinMode(OUT1, OUTPUT);
  digitalWrite(OUT0, LOW);
  digitalWrite(OUT1, LOW);

  //init of serial line
  Serial.begin(115200);
  SerialBuffer.reserve(128);

  
  //init non blocking timer
  timerStart = millis();

  //read histoic data from EEPROM
  for (i = 0; i < 23; i++) {
    EEPROM.get(260 + (i * sizeof(float)), Data5_h[i]);
    if (isnan(Data5_h[i])) {
      Data5_h[i] = 0;
    }
    EEPROM.get(360 + (i * sizeof(float)), Data6_h[i]);
    if (isnan(Data6_h[i])) {
      Data6_h[i] = 0;
    }
  }

 
  //read min and max for AI
  for(i=0;i<4;i++){
    EEPROM.get(50 + (i * 10), AI_min[i]);
    if (isnan(AI_min[i])) 
      AI_min[i] = 0;
    EEPROM.get(55 + (i * 10), AI_max[i]);
    if (isnan(AI_max[i])) 
      AI_max[i] = 100;       
  }

  //read data source of gauges
  DataSource[0] = EEPROM[0];
  DataSource[1] = EEPROM[1];
  DataSource[2] = EEPROM[2];
  DataSource[3] = EEPROM[3];
  DataSource[4] = EEPROM[4];
  DataSource[5] = EEPROM[5];

}

//==================================================================================================
// Main loop
//==================================================================================================

void loop() {

  //  SaveSensorAddres(20);

  //  SendData();

  

  //timer each DELAY_TIME = 100ms
  if ((millis() - timerStart) >= DELAY_TIME) {
    timerStart += DELAY_TIME;

    if (DataIndex > 5)
      DataIndex = 0;
    DataValues[DataIndex] = GetData(DataSource[DataIndex]);
    DataIndex++;

    if (counter > 18000) {  //do every 18k loops = 30 minutes => hostorical data for 12h
      counter = 0;
      for (i = 0; i < 23; i++) {
        Data5_h[i] = Data5_h[i + 1];
        Data6_h[i] = Data6_h[i + 1];
        EEPROM.put(260 + (i * sizeof(float)), Data5_h[i]);
        EEPROM.put(360 + (i * sizeof(float)), Data6_h[i]);
      }
    }

    //each 100ms update last value of the chart
    Data5_h[23] = DataValues[4];
    Data6_h[23] = DataValues[5];

    //keep counter runing
    counter++;
  }

}

/*
  =======================================================================================================================
  Send Data
  =======================================================================================================================

  this is the main function that is sendng all requested data to app.

*/

int SendData() {

  Serial.print("S;");

  for (i = 0; i < 4; i++) {
    Serial.print('D');
    Serial.print((i + 1));
    Serial.print(';');
    if (abs(DataValues[i]) < 10)
      Serial.print(String(DataValues[i], 2));
    else if (abs(DataValues[i]) < 100)
      Serial.print(String(DataValues[i], 1));
    else
      Serial.print(String(DataValues[i], 0));
    Serial.print(';');
  }

  Serial.print("D5;");
  for (i = 0; i < 24; i++) {
    Serial.print(Data5_h[i]);
    Serial.print(';');
  }

  Serial.print("D6;");
  for (i = 0; i < 24; i++) {
    Serial.print(Data6_h[i]);
    Serial.print(';');
  }

  Serial.println("E;");

}


/*
  =======================================================================================================================
  Get Data
  =======================================================================================================================

  this function return data depending on data source indetification (1 for T1, 11 for A1 etc..)
  https://gitlab.com/ardu.camper/arducamper/wikis/EEPROM

*/

float GetData(byte datatype) {
  float data;
  float temp;
  float divider;
  /*long int results[CHANNEL_COUNT];*/

  switch (datatype) {

    case 1:
      data = ReadSensor(10);
      return data;

    case 2:
      data = ReadSensor(20);
      return data;

    case 3:
      data = ReadSensor(30);
      return data;

    case 4:
      data = ReadSensor(40);
      return data;

    case 11:
      temp = ReadSensor(10);
      temp = temp * SET_AI_TEMP_COMP[0];
      data = analogRead(A0);
      data = data / SET_AI_DIVIDERS[0];
      data = data - temp;
      data = data - AI_min[0];
      data = (data / (AI_max[0]-AI_min[0]))*100;
      return data;

    case 12:
      temp = ReadSensor(10);
      temp = temp * SET_AI_TEMP_COMP[1];
      data = analogRead(A1);
      data = data / SET_AI_DIVIDERS[1];
      data = data - temp;
      data = data - AI_min[1];
      data = (data / (AI_max[1]-AI_min[1]))*100;
      return data;

    case 13:
      temp = ReadSensor(10);
      temp = temp * SET_AI_TEMP_COMP[2];
      data = analogRead(A2);
      data = data / SET_AI_DIVIDERS[2];
      data = data - temp;
      data = data - AI_min[2];
      data = (data / (AI_max[2]-AI_min[2]))*100;
      return data;

    case 14:
      temp = ReadSensor(10);
      temp = temp * SET_AI_TEMP_COMP[3];
      data = analogRead(A3);
      data = data / SET_AI_DIVIDERS[3];
      data = data - temp;
      data = data - AI_min[3];
      data = (data / (AI_max[3]-AI_min[3]))*100;
      return data;


    default:
      return 0;
  }
}






/*
  =======================================================================================================================
  Read Sensor
  =======================================================================================================================

  This function read dallas one wire sensor with addres stored in EEPROM from "position" byte[8]
  return float number coresponding to temperature.

*/
float ReadSensor(int position) {
  byte data[12];
  byte addr[8];
  int i;

  for (i = 0; i < 8; i++) {
    addr[i] = EEPROM.read(position + i);
  }

  ds.reset();
  ds.select(addr);
  ds.write(0x44, 1);
  byte present = ds.reset();
  ds.select(addr);
  ds.write(0xBE);
  for (int i = 0; i < 9; i++) { // potřebujeme 9 bytů
    data[i] = ds.read();
  }

  ds.reset_search();

  byte MSB = data[1];
  byte LSB = data[0];

  float tempRead = ((MSB << 8) | LSB);
  float TemperatureSum = tempRead / 16;

  return TemperatureSum;

}



/*
  =======================================================================================================================
  Save Sensor Address
  =======================================================================================================================

  Find the one sensor on one wire bus and save sensor address to EEPROM at position defined by "position"
  Address length is 8 bytes

*/

int SaveSensorAddres(int position) {

  int i;
  byte addr[8];

  if ( !ds.search(addr)) {
    Serial.println("S;X;No device detected;E;");
    ds.reset_search();
    return -1;
  }

  if ( OneWire::crc8( addr, 7) != addr[7]) {
    Serial.println("S;X;CRC is not valid;E;");
    return -1;
  }

  if ( addr[0] != 0x10 && addr[0] != 0x28) {
    Serial.print("S;X;Device is not recognized;E;");
    return -1;
  }

  for (i = 0; i < 8; i++) {
    EEPROM.write(position + i, addr[i]);
  }

  Serial.print("S;OK;E;");

  return 1;
}

/*
  =======================================================================================================================
  serialEvent
  =======================================================================================================================

  Function to handle recieved data. It is waiting for end line character, than it calls function to evaluate the command

*/

void serialEvent() {
  while (Serial.available()) {
    char inChar = (char)Serial.read();
    //check - buffer owerflow
    if (SerialBuffer.length() == 128) {
      Serial.println("S;X;Buffer overflow;E;");
      SerialBuffer = "";
    }

    //add char to buffer
    SerialBuffer += inChar;

    //end of the command - execute it
    if (inChar == '\n') {
      ExecCommand();
    }
  }
}

/*
  =======================================================================================================================
  Exec Command
  =======================================================================================================================

  Function execute command recieved from serial line

*/

int ExecCommand() {
  int C_start = 0;
  int C_stop = 0;
  int C_data = 0;
  char Command = '.';
  float Readout;
  float divider;
  float data;
  float temp;
  /*long int results[CHANNEL_COUNT];*/

  //prepare in string positions
  C_start = SerialBuffer.indexOf("S;") + 2;
  C_data = SerialBuffer.indexOf(';', C_start) + 1;
  C_stop = SerialBuffer.indexOf(";E;", C_start);
  Command = SerialBuffer.charAt(C_start);

  /*
    Serial.println(C_start);
    Serial.println(C_data);
    Serial.println(C_stop);
    Serial.println(Command);
  */

  //check if all positions are OK, if not send error message and finish
  if (C_start < 0 || C_stop < C_start || C_data != (C_start + 2)) {
    Serial.println("S;X;Wrong Format;E;");
    SerialBuffer = "";
    return (0);
  }

  //if there are any data, remove all commands and leave only data in buffer
  if (C_stop > C_data) {
    SerialBuffer.remove(C_stop);
    SerialBuffer.remove(0, C_data);
  }

  switch (Command) {
    case 'R':
      SendData();
      break;
    
    case 'B':
      //read data source of gauges
      DataSource[0] = EEPROM[0];
      DataSource[1] = EEPROM[1];
      DataSource[2] = EEPROM[2];
      DataSource[3] = EEPROM[3];
      DataSource[4] = EEPROM[4];
      DataSource[5] = EEPROM[5]; 
      Serial.println("S;OK;E;");     
      break;

    case 'C':
      for(i=0;i<4;i++){
        AI_min[i]=0;                  
        AI_max[i]=100;                  
        EEPROM.put(50 + (i * 10), 0);
        EEPROM.put(55 + (i * 10), 100);
      }
      Serial.println("S;OK;E;");     
      break;

    case 'V':
        Serial.print("S;B;V");
        Serial.print(FW_VERSION);
        Serial.print(" build at: ");
        Serial.print(FW_BUILD_DATETIME);
        Serial.println(";E;");
        break;

    case 'J':
      int set_output;
      int set_value;
      set_output = SerialBuffer.toInt();
      C_data = SerialBuffer.indexOf(';');
      SerialBuffer.remove(0, C_data + 1);
      set_value = SerialBuffer.toInt();
      if(set_output == 0){
        if(set_value == 1)
          digitalWrite(OUT0, HIGH);
        else        
          digitalWrite(OUT0, LOW);
      Serial.println("S;OK;E;");
      }
      else if(set_output == 1){
        if(set_value == 1)
          digitalWrite(OUT1, HIGH);
        else        
          digitalWrite(OUT1, LOW);
      Serial.println("S;OK;E;");
      }
      else
        Serial.println("S;X;Wrong parameters;E;");
      
      break;

    case 'G':
      int eeprom_address;
      int eeprom_data;
      int eeprom_length;
      eeprom_address = SerialBuffer.toInt();
      C_data = SerialBuffer.indexOf(';');
      SerialBuffer.remove(0, C_data + 1);
      eeprom_length = SerialBuffer.toInt();
      if((eeprom_length > 0) &&  ((eeprom_address+eeprom_length) < 256)){
        Serial.print("S;A;");
        for (i=0; i < eeprom_length; i++) {
          Serial.print(EEPROM[eeprom_address+i]); 
          Serial.print(";");
        }       
        Serial.println("E;");
      }
      else{
        Serial.println("S;X;Wrong parameters;E;");  
      }
      
      break;

    case 'W':
      eeprom_address = SerialBuffer.toInt();
      C_data = SerialBuffer.indexOf(';');
      SerialBuffer.remove(0, C_data + 1);
      eeprom_data = SerialBuffer.toInt();
      EEPROM.write(eeprom_address, eeprom_data);
      Serial.println("S;OK;E;");
      break;

    case 'F':
      temp = ReadSensor(10);
      eeprom_address = SerialBuffer.toInt();
      if (eeprom_address >= 0 && eeprom_address < 4){
        temp = temp * SET_AI_TEMP_COMP[eeprom_address];
        if(eeprom_address == 0)
          data = analogRead(A0);
        else if(eeprom_address == 1)
          data = analogRead(A1);
        else if(eeprom_address == 2)
          data = analogRead(A2);
        else if(eeprom_address == 3)
          data = analogRead(A3);
        Readout = data/SET_AI_DIVIDERS[eeprom_address];
        Readout = Readout - temp;
        EEPROM.put(55 + (eeprom_address*10), Readout);
        AI_max[eeprom_address] = Readout; 
        Serial.println("S;OK;E;");  
      }
      else
        Serial.println("S;X;Wrong parameters;E;");  
       
      break;     

    case 'H':
      temp = ReadSensor(10);
      eeprom_address = SerialBuffer.toInt();
      if (eeprom_address >= 0 && eeprom_address < 4){
        temp = temp * SET_AI_TEMP_COMP[eeprom_address];
        if(eeprom_address == 0)
          data = analogRead(A0);
        else if(eeprom_address == 1)
          data = analogRead(A1);
        else if(eeprom_address == 2)
          data = analogRead(A2);
        else if(eeprom_address == 3)
          data = analogRead(A3);
        Readout = data/SET_AI_DIVIDERS[eeprom_address];
        Readout = Readout - temp;
        EEPROM.put(50 + (eeprom_address*10), Readout);
        AI_min[eeprom_address] = Readout; 
        Serial.println("S;OK;E;");  
      }
      else{
        Serial.println("S;X;Wrong parameters;E;");  
      }      
      break;

    case 'I':
      eeprom_address = SerialBuffer.toInt();
      if (eeprom_address > 0 && eeprom_address < 5){
        eeprom_address = (eeprom_address*10);
        SaveSensorAddres(eeprom_address);
      }
      else{
        Serial.println("S;X;Wrong parameters;E;");  
      }      
      break;


    default:
      Serial.println("S;X;Unknown command;E;");
      break;
  }

  SerialBuffer = "";

}
